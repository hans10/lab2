**Goal:** Automatically deploy each change that appears on the master 
branch to the target environment previously created.

# Prerequisites

- You have provisioned a target Elastic Beanstalk environment
- You have created an API-key for IAM user `ci`.

# The excersise

1. Add a job called 'deploy' to [.gitlab-ci.yml](). A few hints:
Make sure it uses the `image` that is already in there. This will provide the job with an execution context

2. Deployment must be performed by executing `eb init` and `eb deploy`

3. You will have to configure the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` 
environment variables. Use the values for user `ci`. Do not use root-credentials.

# Questions for you to think about
- How would you treat feature-branches?
- How would you add other environments, such as `staging`?
- How many different DSLs for CI/CD tools have you used? 😂😂😂
